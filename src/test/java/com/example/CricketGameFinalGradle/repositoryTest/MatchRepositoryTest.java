package com.example.CricketGameFinalGradle.repositoryTest;

import com.example.CricketGameFinalGradle.models.Match;
import com.example.CricketGameFinalGradle.models.ScoreCard;
import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.repository.mongorepo.MatchRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.core.FindAndModifyOptions;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DataMongoTest
public class MatchRepositoryTest {
    @Mock
    private MongoTemplate mongoTemplate;
    @InjectMocks
    private MatchRepository matchRepository;

    @Test
    public void testCreateMatch() throws Exception{
        Match match=new Match();
        match.setId(2);
        match.setGameFormat("T20");
        match.setFirstTeamScoreCard(new ScoreCard());
        match.setSecondTeamScoreCard(new ScoreCard());
        match.setWinnerTeamId(2);
        match.setFirstTeamId(3);
        match.setSecondTeamId(4);
        Mockito.when(matchRepository.createMatch(match)).thenReturn(match);
        Match result=matchRepository.createMatch(match);
        assertEquals(result,match);

    }
    @Test
    public void testGetMatchById() throws Exception{
        Match match=new Match();
        match.setId(2);
        match.setGameFormat("T20");
        match.setFirstTeamScoreCard(new ScoreCard());
        match.setSecondTeamScoreCard(new ScoreCard());
        match.setWinnerTeamId(2);
        match.setFirstTeamId(3);
        match.setSecondTeamId(4);
        Mockito.when(matchRepository.getMatchById(match.getId())).thenReturn(match);
        Match result=matchRepository.getMatchById(match.getId());
        assertEquals(result,match);

    }
    @Test
    public void testGetWinningTeam() throws Exception{
        Match mockedMatch =new Match();
        mockedMatch.setId(2);
        mockedMatch.setGameFormat("T20");
        mockedMatch.setFirstTeamScoreCard(new ScoreCard());
        mockedMatch.setSecondTeamScoreCard(new ScoreCard());
        mockedMatch.setWinnerTeamId(2);
        mockedMatch.setFirstTeamId(3);
        mockedMatch.setSecondTeamId(4);
        Team mockedTeam=new Team();
        mockedTeam.setTeamId(4);
        mockedTeam.setTeamScore(200);
        mockedTeam.setTeamName("India");
        mockedTeam.setPlayers(new ArrayList<>());
        mockedTeam.setBowlers(new ArrayList<>());
        mockedTeam.setTotalWicketLoss(9);
        mockedTeam.setTotalOversPlayed(20);
        when(mongoTemplate.findOne(any(Query.class), eq(Match.class))).thenReturn(mockedMatch);
        when(mongoTemplate.findOne(any(Query.class), eq(Team.class))).thenReturn(mockedTeam);

        Team winningTeam = matchRepository.getWinningTeam(1);

        assertEquals(4, winningTeam.getTeamId());

    }
    @Test
    public void testSetMatchWinnerAndScoreCard() throws Exception{
        Match mockedMatch =new Match();
        mockedMatch.setId(2);
        mockedMatch.setGameFormat("T20");
        mockedMatch.setFirstTeamScoreCard(new ScoreCard());
        mockedMatch.setSecondTeamScoreCard(new ScoreCard());
        mockedMatch.setWinnerTeamId(2);
        mockedMatch.setFirstTeamId(3);
        mockedMatch.setSecondTeamId(4);
        when(mongoTemplate.findAndModify(any(Query.class), any(Update.class), any(FindAndModifyOptions.class), eq(Match.class))).thenReturn(mockedMatch);
        Match updatedMatch = matchRepository.setMatchWinnerAndScoreCard(1, 1, new ScoreCard(), new ScoreCard());
        assertEquals(2, updatedMatch.getId().intValue());
        assertEquals(2 , updatedMatch.getWinnerTeamId().intValue());
        assertEquals(new ScoreCard(), updatedMatch.getFirstTeamScoreCard());
        assertEquals(new ScoreCard(), updatedMatch.getSecondTeamScoreCard());

    }

}

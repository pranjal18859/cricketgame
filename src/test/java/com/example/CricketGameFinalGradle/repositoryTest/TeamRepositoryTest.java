package com.example.CricketGameFinalGradle.repositoryTest;

import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.repository.mongorepo.TeamRepository;
import com.example.CricketGameFinalGradle.services.serviceimplementation.TeamBuilderImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TeamRepositoryTest {
    @Mock
    private MongoTemplate mongoTemplate;
    @Mock
    private TeamBuilderImpl mockTeamBuilder;
    @InjectMocks
    private TeamRepository teamRepository;



    @Test
    public void testGetTeamById() {
        Team mockTeam = new Team();
        mockTeam.setTeamName("India");
        mockTeam.setTeamScore(390);
        mockTeam.setTeamId(5);
        mockTeam.setTeamId(2);
        mockTeam.setTotalWicketLoss(8);
        mockTeam.setTotalOversPlayed(50);
        when(mongoTemplate.findOne(any(Query.class), eq(Team.class))).thenReturn(mockTeam);
        Team team = teamRepository.getTeamById(1);
        verify(mongoTemplate).findOne(eq(new Query(Criteria.where("teamId").is(1))), eq(Team.class));
        assertEquals(mockTeam, team);
    }

}

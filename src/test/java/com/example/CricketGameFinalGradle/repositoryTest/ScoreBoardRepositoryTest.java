package com.example.CricketGameFinalGradle.repositoryTest;

import com.example.CricketGameFinalGradle.models.ScoreCard;
import com.example.CricketGameFinalGradle.repository.mongorepo.ScorecardRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ScoreBoardRepositoryTest {
    @Mock
    private MongoTemplate mongoTemplate;
    @InjectMocks
    private ScorecardRepository scorecardRepository;
    @Test
    public void testSetScoreCard() throws Exception{
        ScoreCard scoreCard = new ScoreCard();

        when(mongoTemplate.save(scoreCard)).thenReturn(scoreCard);

        ScoreCard savedScoreCard = scorecardRepository.setScoreCard(scoreCard);
        assertEquals(scoreCard, savedScoreCard);

        verify(mongoTemplate, times(1)).save(scoreCard);
    }
}

package com.example.CricketGameFinalGradle.repositoryTest;

import com.example.CricketGameFinalGradle.enums.PlayerRole;
import com.example.CricketGameFinalGradle.models.Player;
import com.example.CricketGameFinalGradle.repository.mongorepo.PlayerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PlayerRepositoryTest {
    @Mock
    private MongoTemplate mongoTemplate;
    @InjectMocks
    private PlayerRepository playerRepository;
    @Test
    public void testGetPlayerById() throws Exception{
        Player player=new Player();
        player.setPlayerName("Pranjal");
        player.setPlayerRole(PlayerRole.BATSMAN);
        player.setId(4);
        player.setWicketsTaken(2);
        player.setRunsScored(132);
        player.setBallsFaced(34);
        player.setTeamId(2);
        player.setRunsGiven(24);
        Query query = new Query(Criteria.where("id").is(4));
        when(mongoTemplate.findOne(query,Player.class)).thenReturn(player);
        Player result=playerRepository.getPlayerById(4);
        assertEquals(player, result);
        verify(mongoTemplate).findOne(query, Player.class);
    }
    @Test
    public void testAddPlayer() {
        Player player=new Player();
        player.setPlayerName("Pranjal");
        player.setPlayerRole(PlayerRole.BATSMAN);
        player.setId(4);
        player.setWicketsTaken(2);
        player.setRunsScored(132);
        player.setBallsFaced(34);
        player.setTeamId(2);
        player.setRunsGiven(24);
        when(mongoTemplate.save(player)).thenReturn(player);
        Player result = playerRepository.addPlayer(player);
        assertEquals(player, result);
        verify(mongoTemplate).save(player);
    }
    @Test
    public void testGetAllPlayers() {
        Player player=new Player();
        player.setPlayerName("Pranjal");
        player.setPlayerRole(PlayerRole.BATSMAN);
        player.setId(4);
        player.setWicketsTaken(2);
        player.setRunsScored(132);
        player.setBallsFaced(34);
        player.setTeamId(2);
        player.setRunsGiven(24);
        List<Player>players=new ArrayList<>();
        players.add(player);
        when(mongoTemplate.findAll(Player.class)).thenReturn(players);
        List<Player> result = playerRepository.getAllPlayers();
        assertEquals(players, result);
        verify(mongoTemplate).findAll(Player.class);
    }
    @Test
    public void testAddMultiplePlayers() {
        Player player=new Player();
        player.setPlayerName("Pranjal");
        player.setPlayerRole(PlayerRole.BATSMAN);
        player.setId(4);
        player.setWicketsTaken(2);
        player.setRunsScored(132);
        player.setBallsFaced(34);
        player.setTeamId(2);
        player.setRunsGiven(24);
        Player player1=new Player();
        player1.setPlayerName("Piyush");
        player1.setPlayerRole(PlayerRole.BOWLER);
        player1.setId(5);
        player1.setWicketsTaken(3);
        player1.setRunsScored(13);
        player1.setBallsFaced(34);
        player1.setTeamId(3);
        player1.setRunsGiven(29);
        List<Player> players = new ArrayList<>();
        when(mongoTemplate.insertAll(players)).thenReturn(players);
        Collection<Player> result = playerRepository.addMultiplePlayers(players);
        assertEquals(players, result);
        verify(mongoTemplate).insertAll(players);
    }


}

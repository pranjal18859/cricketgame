package com.example.CricketGameFinalGradle.controllerTest;

import com.example.CricketGameFinalGradle.controller.TeamController;
import com.example.CricketGameFinalGradle.models.Match;
import com.example.CricketGameFinalGradle.services.serviceimplementation.MatchDetailsServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.hamcrest.Matchers.is;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isNull;

@WebMvcTest(TeamController.class)
public class MatchDetailsControllerTest {
    @Autowired
    private MockMvc mockMvc;
    private final ObjectMapper objectMapper=new ObjectMapper();
    private final ObjectWriter objectWriter= objectMapper.writer();

    @MockBean
    private MatchDetailsServiceImpl matchDetailsServiceImpl;

    @Test
    public void testGetWinningTeam()throws Exception {
        Match match=new Match();
        match.setWinnerTeamId(2);
        String content= objectWriter.writeValueAsString(match);
        MockHttpServletRequestBuilder mockHttpServletRequestBuilder= MockMvcRequestBuilders.get("/cricketGame/getWinningTeam?matchId=2")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(content);
        mockMvc.perform(mockHttpServletRequestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.winningTeamId",is(2)));

    }

}

package com.example.CricketGameFinalGradle.controllerTest;

import com.example.CricketGameFinalGradle.controller.MatchController;
import com.example.CricketGameFinalGradle.models.Match;
import com.example.CricketGameFinalGradle.models.ScoreCard;
import com.example.CricketGameFinalGradle.services.serviceimplementation.GameServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;


//@ExtendWith(MockitoExtension.class)
public class MatchControllerTest {

    private MockMvc mockMvc;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final ObjectWriter objectWriter = objectMapper.writer();

    @Mock
    private GameServiceImpl gameServiceImpl;

    @InjectMocks
    private MatchController matchController;

    ScoreCard scoreCard1 = new ScoreCard();
    ScoreCard scoreCard2 = new ScoreCard();

    Match match_1 = Match.builder()
            .id(2)
            .firstTeamId(4)
            .secondTeamId(5)
            .gameFormat("T20").build();



    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(matchController).build();
    }

    @Test
    public void TestCreateMatch_Success() throws Exception {


        Mockito.when(gameServiceImpl.gameService(match_1)).thenReturn("Game has completed");
        String content = objectWriter.writeValueAsString(match_1);
        MockHttpServletRequestBuilder mockHttpServletRequestBuilder = MockMvcRequestBuilders
                .post("/cricketGame/createMatch")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(content);
        mockMvc.perform(mockHttpServletRequestBuilder)
                .andDo(print());
//                .andExpect(status().isOk());
//                .andExpect(MockMvcResultMatchers.jsonPath("$", notNullValue()))
//                .andExpect(MockMvcResultMatchers.jsonPath("$", is("Match Done")));
//                .andExpect(MockMvcResultMatchers.jsonPath("$.gameFormat", is("T20")))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.firstTeamId", is(4)));
    }

@Test
void contextLoad() {
    }
}



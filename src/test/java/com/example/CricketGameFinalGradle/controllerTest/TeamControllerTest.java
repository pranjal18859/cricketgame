package com.example.CricketGameFinalGradle.controllerTest;

import com.example.CricketGameFinalGradle.controller.TeamController;
import com.example.CricketGameFinalGradle.enums.PlayerRole;
import com.example.CricketGameFinalGradle.models.Player;
import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.services.serviceimplementation.TeamServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(TeamController.class)
public class TeamControllerTest {
    @Autowired
    private MockMvc mockMvc;
    private final ObjectMapper objectMapper=new ObjectMapper();
    private final ObjectWriter objectWriter= objectMapper.writer();
    @MockBean
    private TeamServiceImpl teamServiceImpl;
    @Test
    public void testCreateTeam() throws Exception{
        Team team=new Team();
        team.setTeamId(4);
        team.setTeamName("India");
        team.setTeamScore(287);
        team.setTotalOversPlayed(39);
        team.setTotalWicketLoss(9);
        ArrayList<Player> bowlers = new ArrayList<>();
        bowlers.add(new Player("Jadeja", PlayerRole.BOWLER, 2, 4, 4, 31, 49));
        team.setBowlers(bowlers);
        ArrayList<Player> players = new ArrayList<>();
        players.add(new Player("Hardik", PlayerRole.BATSMAN, 23, 45, 1, 3, 56));
        team.setPlayers(players);
        Mockito.when(teamServiceImpl.createTeamById("India",4)).thenReturn(team);
        String content= objectWriter.writeValueAsString(team);
        MockHttpServletRequestBuilder mockHttpServletRequestBuilder=MockMvcRequestBuilders.post("/cricketGame/createTeam?id=4")
                .param("teamName", team.getTeamName())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(content);
        mockMvc.perform(mockHttpServletRequestBuilder)
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.teamName",is("India")));











    }


}

package com.example.CricketGameFinalGradle.controllerTest;

import com.example.CricketGameFinalGradle.controller.PlayerController;
import com.example.CricketGameFinalGradle.enums.PlayerRole;
import com.example.CricketGameFinalGradle.models.Player;
import com.example.CricketGameFinalGradle.services.serviceimplementation.PlayerServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;


@WebMvcTest(PlayerController.class)
public class PlayerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper objectMapper=new ObjectMapper();
    private final ObjectWriter objectWriter= objectMapper.writer();
    @MockBean
    private PlayerServiceImpl playerService;
    @Test
    public void TestGetPlayerById_Success() throws Exception{
        Player player_1=new Player();
        player_1.setPlayerName("Virat");
        player_1.setId(1);
        player_1.setTeamId(4);
        player_1.setRunsGiven(22);
        player_1.setRunsScored(76);
        player_1.setBallsFaced(43);
        player_1.setWicketsTaken(2);

        when(playerService.getPlayerById(1)).thenReturn(player_1);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/cricketGame/getPlayerById/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.playerName", is("Virat")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.teamId", is(4)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.runsScored", is(76)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.ballsFaced", is(43)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.wicketsTaken", is(2)));


    }
    @Test
    public void testAddPlayer() throws Exception{
        Player player_2=new Player();
        player_2.setPlayerName("Ronit");
        player_2.setId(2);
        player_2.setTeamId(5);
        player_2.setRunsGiven(21);
        player_2.setRunsScored(100);
        player_2.setBallsFaced(49);
        player_2.setWicketsTaken(3);
        when(playerService.addPlayer(player_2)).thenReturn(player_2);
        String content=objectWriter.writeValueAsString(player_2);
        MockHttpServletRequestBuilder mockHttpServletRequestBuilder=MockMvcRequestBuilders.post("/cricketGame/addPlayer")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(content);
        mockMvc.perform(mockHttpServletRequestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.playerName",is("Ronit")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.runsGiven",is(21)));

    }
    @Test
    public void testAddMultiplePlayers() throws Exception{
        List<Player> playerList= Arrays.asList(
                new Player("Piyush",PlayerRole.BATSMAN,23,45,1,5,45)
        );
        Mockito.when(playerService.addMultiplePlayers(playerList)).thenReturn(playerList);
        String content= objectWriter.writeValueAsString(playerList);
        MockHttpServletRequestBuilder mockHttpServletRequestBuilder=MockMvcRequestBuilders.post("/cricketGame/addMultiplePlayers")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(content);
        mockMvc.perform(mockHttpServletRequestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(1)));
    }
    @Test
    public void testGetAllPlayers() throws Exception{
        List<Player> playerList_1=Arrays.asList(
                new Player("Pranjal",PlayerRole.BOWLER,3,4,5,6,7),
                new Player("Sumit",PlayerRole.BATSMAN,10,9,8,7,6)
        );
        Mockito.when(playerService.getAllPlayers()).thenReturn(playerList_1);
        String content= objectWriter.writeValueAsString(playerList_1);
        MockHttpServletRequestBuilder mockHttpServletRequestBuilder=MockMvcRequestBuilders.get("/cricketGame/getAllPlayers")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(content);
        mockMvc.perform(mockHttpServletRequestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(2)));


    }

}



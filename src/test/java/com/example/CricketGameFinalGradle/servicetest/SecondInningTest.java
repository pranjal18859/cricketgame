package com.example.CricketGameFinalGradle.servicetest;

import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.services.serviceimplementation.GenerateRunsImpl;
import com.example.CricketGameFinalGradle.services.serviceimplementation.SecondInningImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SecondInningTest {
    @MockBean
    private GenerateRunsImpl generateRunsImpl;
    @Autowired
    private SecondInningImpl secondInningImpl;

    @Test
    void testStartTheSecondInning() {

        when(generateRunsImpl.generateRandom()).thenReturn(5);

        Team firstTeam = new Team();
        Team secondTeam = new Team();
        int overs = 10;
        int targetScore = 50;
        int totalScore = secondInningImpl.startTheSecondInning(firstTeam, secondTeam, overs, targetScore);
        assertEquals(60, totalScore);
    }

}

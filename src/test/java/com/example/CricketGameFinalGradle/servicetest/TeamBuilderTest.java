package com.example.CricketGameFinalGradle.servicetest;

import com.example.CricketGameFinalGradle.enums.PlayerRole;
import com.example.CricketGameFinalGradle.models.Player;
import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.services.serviceimplementation.PlayerServiceImpl;
import com.example.CricketGameFinalGradle.services.servicesinterface.TeamBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class TeamBuilderTest {
    @Autowired
    TeamBuilder teamBuilder;
    @Mock
    private PlayerServiceImpl playerService;
    @Test
    public void testTeamBuilder() throws Exception{
        Player player1=new Player();
        player1.setPlayerName("Pranjal");
        player1.setPlayerRole(PlayerRole.BATSMAN);
        player1.setId(4);
        player1.setWicketsTaken(2);
        player1.setRunsScored(132);
        player1.setBallsFaced(34);
        player1.setTeamId(2);
        player1.setRunsGiven(24);
        Player player2=new Player();
        player2.setPlayerName("Piyush");
        player2.setPlayerRole(PlayerRole.BOWLER);
        player2.setId(5);
        player2.setWicketsTaken(1);
        player2.setRunsScored(13);
        player2.setBallsFaced(30);
        player2.setTeamId(1);
        player2.setRunsGiven(28);
        ArrayList<Player> playerList= new ArrayList<>();
        playerList.add(player1);
        playerList.add(player2);
        Mockito.when(playerService.getAllPlayers()).thenReturn(playerList);
        Team team=new Team();
        team.setTeamName("India");
        team.setPlayers(playerList);
        assertEquals(1, team.getTeamId());
        assertEquals("India", team.getTeamName());
    }


}

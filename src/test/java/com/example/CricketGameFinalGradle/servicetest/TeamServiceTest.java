package com.example.CricketGameFinalGradle.servicetest;

import com.example.CricketGameFinalGradle.enums.PlayerRole;
import com.example.CricketGameFinalGradle.models.Player;
import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.repository.mongorepo.TeamRepository;
import com.example.CricketGameFinalGradle.services.serviceimplementation.TeamServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class TeamServiceTest {


    @Mock
    private TeamRepository teamRepository;
    @InjectMocks
    private TeamServiceImpl teamService;

    @Test
    public void testGetTeamById() throws Exception {
        Team team=new Team();
        team.setTeamName("India");
        team.setTeamScore(390);
        team.setTeamId(5);
        team.setTeamId(2);
        team.setTotalWicketLoss(8);
        team.setTotalOversPlayed(50);
        Player player=new Player();
        player.setPlayerName("Pranjal");
        player.setPlayerRole(PlayerRole.BATSMAN);
        player.setId(4);
        player.setWicketsTaken(2);
        player.setRunsScored(132);
        player.setBallsFaced(34);
        player.setTeamId(2);
        player.setRunsGiven(24);
        ArrayList<Player> playerArrayList=new ArrayList<>();
        playerArrayList.add(player);
        team.setPlayers(playerArrayList);
        Player player2=new Player();
        player2.setPlayerName("Piyush");
        player2.setPlayerRole(PlayerRole.BOWLER);
        player2.setId(5);
        player2.setWicketsTaken(1);
        player2.setRunsScored(13);
        player2.setBallsFaced(30);
        player2.setTeamId(1);
        player2.setRunsGiven(28);
        ArrayList<Player> bowlers=new ArrayList<>();
        bowlers.add(player2);
        team.setBowlers(bowlers);
        Mockito.when(teamRepository.getTeamById(1)).thenReturn(team);
        Team result = teamService.getTeamById(1);

        assertEquals(team, result);
    }
    @Test
    public void testCreateTeamById() throws Exception{
        Team team=new Team();
        team.setTeamName("India");
        team.setTeamScore(390);
        team.setTeamId(5);
        team.setTeamId(2);
        team.setTotalWicketLoss(8);
        team.setTotalOversPlayed(50);
        Player player=new Player();
        player.setPlayerName("Pranjal");
        player.setPlayerRole(PlayerRole.BATSMAN);
        player.setId(4);
        player.setWicketsTaken(2);
        player.setRunsScored(132);
        player.setBallsFaced(34);
        player.setTeamId(2);
        player.setRunsGiven(24);
        ArrayList<Player> playerArrayList=new ArrayList<>();
        playerArrayList.add(player);
        team.setPlayers(playerArrayList);
        Player player2=new Player();
        player2.setPlayerName("Piyush");
        player2.setPlayerRole(PlayerRole.BOWLER);
        player2.setId(5);
        player2.setWicketsTaken(1);
        player2.setRunsScored(13);
        player2.setBallsFaced(30);
        player2.setTeamId(1);
        player2.setRunsGiven(28);
        ArrayList<Player> bowlers=new ArrayList<>();
        bowlers.add(player2);
        team.setBowlers(bowlers);
        Mockito.when(teamRepository.createTeamById(team.getTeamName(),team.getTeamId())).thenReturn(team);
        Team result=teamService.createTeamById(team.getTeamName(),team.getTeamId());
        assertEquals(team,result);


    }

}

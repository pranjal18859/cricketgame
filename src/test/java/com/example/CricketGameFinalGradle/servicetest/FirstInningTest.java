package com.example.CricketGameFinalGradle.servicetest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

import com.example.CricketGameFinalGradle.models.Player;
import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.services.serviceimplementation.FirstInningImpl;
import com.example.CricketGameFinalGradle.services.serviceimplementation.GenerateRunsImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
@ExtendWith(MockitoExtension.class)

public class FirstInningTest {

    private  FirstInningImpl firstInningImpl;
    private GenerateRunsImpl generateRunsImpl;

    @BeforeEach
    public void setup() {
        firstInningImpl = new FirstInningImpl();
        generateRunsImpl = mock(GenerateRunsImpl.class);
    }

    @Test
    public void testStartTheFirstInning() {
        Team firstTeam = new Team();
        Team secondTeam = new Team();

        Player player1 = new Player();
        Player player2 = new Player();

        firstTeam.setPlayers(new ArrayList<>(List.of(player1,player2)));

        Player bowler1 = new Player();
        Player bowler2 = new Player();
        secondTeam.setBowlers(new ArrayList<>(List.of(bowler1, bowler2)));
        assertEquals(2,firstTeam.getPlayers().size());
    }
}

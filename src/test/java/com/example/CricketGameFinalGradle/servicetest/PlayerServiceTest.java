package com.example.CricketGameFinalGradle.servicetest;


import com.example.CricketGameFinalGradle.enums.PlayerRole;
import com.example.CricketGameFinalGradle.models.Player;
import com.example.CricketGameFinalGradle.repository.mongorepo.PlayerRepository;
import com.example.CricketGameFinalGradle.services.serviceimplementation.PlayerServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PlayerServiceTest {
    @Mock
    private PlayerRepository playerRepository;
    @InjectMocks
    private PlayerServiceImpl playerServiceImpl;
    @Test
    public void testGetPlayerById() throws Exception{
        Player player=new Player();
        player.setPlayerName("Pranjal");
        player.setPlayerRole(PlayerRole.BATSMAN);
        player.setId(4);
        player.setWicketsTaken(2);
        player.setRunsScored(132);
        player.setBallsFaced(34);
        player.setTeamId(2);
        player.setRunsGiven(24);
        Mockito.when(playerRepository.getPlayerById(player.getId())).thenReturn(player);
        Player player1=playerServiceImpl.getPlayerById(player.getId());
        assertEquals(4,player1.getId());
    }
    @Test
    public void testAddPlayer() throws Exception{
        Player player=new Player();
        player.setPlayerName("Pranjal");
        player.setPlayerRole(PlayerRole.BATSMAN);
        player.setId(4);
        player.setWicketsTaken(2);
        player.setRunsScored(132);
        player.setBallsFaced(34);
        player.setTeamId(2);
        player.setRunsGiven(24);
        Mockito.when(playerRepository.addPlayer(player)).thenReturn(player);
        Player savedPlayer=playerServiceImpl.addPlayer(player);
        verify(playerRepository, times(1)).addPlayer(player);
        assertEquals(player, savedPlayer);
    }
    @Test
    public void testAddMultiplePlayers() throws Exception{
        List<Player> playerList=new ArrayList<>();
        Player player1=new Player();
        player1.setPlayerName("Pranjal");
        player1.setPlayerRole(PlayerRole.BATSMAN);
        player1.setId(4);
        player1.setWicketsTaken(2);
        player1.setRunsScored(132);
        player1.setBallsFaced(34);
        player1.setTeamId(2);
        player1.setRunsGiven(24);
        Player player2=new Player();
        player2.setPlayerName("Piyush");
        player2.setPlayerRole(PlayerRole.BOWLER);
        player2.setId(5);
        player2.setWicketsTaken(1);
        player2.setRunsScored(13);
        player2.setBallsFaced(30);
        player2.setTeamId(1);
        player2.setRunsGiven(28);
        playerList.add(player1);
        playerList.add(player2);
        Mockito.when(playerRepository.addMultiplePlayers(playerList)).thenReturn(playerList);
        Collection<Player> savedPlayers = playerServiceImpl.addMultiplePlayers(playerList);
        verify(playerRepository, times(1)).addMultiplePlayers(playerList);
        assertEquals(playerList, savedPlayers);
    }
    @Test
    public void testGetAllPlayers() throws Exception{
        List<Player> playerList=new ArrayList<>();
        Player player1=new Player();
        player1.setPlayerName("Pranjal");
        player1.setPlayerRole(PlayerRole.BATSMAN);
        player1.setId(4);
        player1.setWicketsTaken(2);
        player1.setRunsScored(132);
        player1.setBallsFaced(34);
        player1.setTeamId(2);
        player1.setRunsGiven(24);
        Player player2=new Player();
        player2.setPlayerName("Piyush");
        player2.setPlayerRole(PlayerRole.BOWLER);
        player2.setId(5);
        player2.setWicketsTaken(1);
        player2.setRunsScored(13);
        player2.setBallsFaced(30);
        player2.setTeamId(1);
        player2.setRunsGiven(28);
        playerList.add(player1);
        playerList.add(player2);
        Mockito.when(playerRepository.getAllPlayers()).thenReturn(playerList);
        Collection<Player> savedPlayers=playerServiceImpl.getAllPlayers();
        assertEquals(playerList,savedPlayers);

    }









}

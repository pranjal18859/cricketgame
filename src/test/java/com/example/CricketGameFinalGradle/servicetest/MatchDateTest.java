package com.example.CricketGameFinalGradle.servicetest;

import com.example.CricketGameFinalGradle.services.servicesinterface.MatchDate;
import com.example.CricketGameFinalGradle.services.serviceimplementation.MatchDateImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)

public class MatchDateTest {

    @Test
    public void testDisplayMatchDate() {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        MatchDate matchDate = new MatchDateImpl();
        matchDate.displayMatchDate();
        System.setOut(System.out);

        String output = outputStream.toString().trim();
        String regex = "\\d{2}/\\d{2}/\\d{4} \\d{2}:\\d{2}:\\d{2}";
        assertTrue(output.matches(regex), "Match date output does not match expected format");
    }
}

package com.example.CricketGameFinalGradle.servicetest;

import com.example.CricketGameFinalGradle.enums.PlayerRole;
import com.example.CricketGameFinalGradle.models.Player;
import com.example.CricketGameFinalGradle.services.serviceimplementation.StrikeChangeImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class StrikeChangeTest {
    private StrikeChangeImpl strikeChange=new StrikeChangeImpl();
    @Test
    public void testStrikeChange() throws Exception{
        Player player1=new Player();
        player1.setPlayerName("Pranjal");
        player1.setPlayerRole(PlayerRole.BATSMAN);
        player1.setId(4);
        player1.setWicketsTaken(2);
        player1.setRunsScored(132);
        player1.setBallsFaced(34);
        player1.setTeamId(2);
        player1.setRunsGiven(24);
        Player player2=new Player();
        player2.setPlayerName("Piyush");
        player2.setPlayerRole(PlayerRole.BOWLER);
        player2.setId(5);
        player2.setWicketsTaken(1);
        player2.setRunsScored(13);
        player2.setBallsFaced(30);
        player2.setTeamId(1);
        player2.setRunsGiven(28);
        ArrayList<Player> result=strikeChange.changeStrike(player1,player2);
        assertEquals(2,result.size());
        assertEquals(player2,result.get(0));
        assertEquals(player1,result.get(1));



    }


}

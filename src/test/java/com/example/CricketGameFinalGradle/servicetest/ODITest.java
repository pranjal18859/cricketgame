package com.example.CricketGameFinalGradle.servicetest;

import com.example.CricketGameFinalGradle.services.serviceimplementation.ODI;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class ODITest {
    @Test
    public void testGetMatchFormat() throws Exception {
        ODI odi = new ODI();
        int expectedFormat = 50;
        int actualFormat = odi.getmatchFormat();
        assertEquals(expectedFormat, actualFormat);
    }

}

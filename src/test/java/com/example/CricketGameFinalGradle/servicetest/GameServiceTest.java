package com.example.CricketGameFinalGradle.servicetest;

import com.example.CricketGameFinalGradle.models.Match;
import com.example.CricketGameFinalGradle.models.ScoreCard;
import com.example.CricketGameFinalGradle.services.serviceimplementation.GameServiceImpl;
import com.example.CricketGameFinalGradle.services.serviceimplementation.MatchStarter;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameServiceTest {

    @Mock
    private MatchStarter matchStarter;

    @InjectMocks
    private GameServiceImpl gameService;

    @Test
    public void testGameService() {
        Match match = new Match();
        ScoreCard scoreCard1=new ScoreCard();
        ScoreCard scoreCard2=new ScoreCard();
        match.setFirstTeamId(4);
        match.setId(13);
        match.setWinnerTeamId(2);
        match.setGameFormat("T20");
        match.setSecondTeamId(7);
        match.setFirstTeamScoreCard(scoreCard1);
        match.setSecondTeamScoreCard(scoreCard2);
        Mockito.doNothing().when(matchStarter).play(match);
        String result = gameService.gameService(match);
        Assert.assertEquals("Game has completed", result);
    }
}


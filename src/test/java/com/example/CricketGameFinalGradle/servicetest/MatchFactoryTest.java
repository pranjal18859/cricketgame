package com.example.CricketGameFinalGradle.servicetest;

import com.example.CricketGameFinalGradle.services.serviceimplementation.DuckworthLewis;
import com.example.CricketGameFinalGradle.services.serviceimplementation.MatchFactory;
import com.example.CricketGameFinalGradle.services.serviceimplementation.ODI;
import com.example.CricketGameFinalGradle.services.serviceimplementation.T20;
import com.example.CricketGameFinalGradle.services.servicesinterface.MatchFormat;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class MatchFactoryTest {

    @Test
    public void testGetMatchFormat() throws Exception {
        MatchFactory matchFactory = new MatchFactory();
        MatchFormat matchFormatT20 = matchFactory.getmatchFormat("T20");
        MatchFormat matchFormatODI = matchFactory.getmatchFormat("ODI");
        MatchFormat matchFormatRain = matchFactory.getmatchFormat("RAIN");

        assertTrue(matchFormatT20 instanceof T20, "Returned object should be instance of T20");
        assertTrue(matchFormatODI instanceof ODI, "Returned object should be instance of ODI");
        assertTrue(matchFormatRain instanceof DuckworthLewis, "Returned object should be instance of DuckworthLewis");


    }
}

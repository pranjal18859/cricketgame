package com.example.CricketGameFinalGradle.servicetest;

import com.example.CricketGameFinalGradle.services.serviceimplementation.TossServicesImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)

public class TossTest {
    @Test
    void testToss() {
        int result = TossServicesImpl.toss();
        assertTrue(result == 0 || result == 1);
    }
}

package com.example.CricketGameFinalGradle.servicetest;

import com.example.CricketGameFinalGradle.services.serviceimplementation.DuckworthLewis;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class DuckworthLewisTest {
    @Test
    public void getMatchFormatShouldReturnRandomNumberBetween0And50() throws Exception{
        DuckworthLewis match = new DuckworthLewis();
        int result = match.getmatchFormat();
        assertTrue(result >= 0 && result <= 50, "Result should be between 0 and 50");
    }

}

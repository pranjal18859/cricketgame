package com.example.CricketGameFinalGradle.servicetest;

import com.example.CricketGameFinalGradle.services.serviceimplementation.AnimationImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class AnimationTest {
    @Test
    public void testPrintMsgWithProgressBar() {
        String message = "Loading...";
        int length = 10;
        long timeInterval = 100L;
        AnimationImpl.printMsgWithProgressBar(message, length, timeInterval);
        String expectedOutput = String.format("%s%n%s", message, "██████████");
        String actualOutput = "Loading...\n██████████";
        Assertions.assertEquals(expectedOutput, actualOutput);
    }
}

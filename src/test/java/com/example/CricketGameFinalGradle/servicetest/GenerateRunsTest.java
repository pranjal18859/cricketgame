package com.example.CricketGameFinalGradle.servicetest;

import com.example.CricketGameFinalGradle.services.serviceimplementation.GenerateRunsImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class GenerateRunsTest {
    private GenerateRunsImpl generateRuns=new GenerateRunsImpl();

    @Test
    public void testGenerateRandom() {
        int result = generateRuns.generateRandom();
        assertTrue(result >= 0 && result <= 7, "Generated result should be between 0 and 7");


    }
}

package com.example.CricketGameFinalGradle.servicetest;

import com.example.CricketGameFinalGradle.models.PlayerScoreInAMatch;
import com.example.CricketGameFinalGradle.models.ScoreCard;
import com.example.CricketGameFinalGradle.repository.mongorepo.ScorecardRepository;
import com.example.CricketGameFinalGradle.services.serviceimplementation.ScorecardServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class ScoreCardServiceTest {
    @Mock
    private ScorecardRepository scorecardRepository;
    @InjectMocks
    private ScorecardServiceImpl scorecardService;
    @Test
    public void testScoreCard() throws Exception{
        List<PlayerScoreInAMatch> playerScoreInAMatchList=new ArrayList<>();
        PlayerScoreInAMatch playerScoreInAMatch=new PlayerScoreInAMatch();
        playerScoreInAMatch.setRunsScored(50);
        playerScoreInAMatch.setPlayerId(6);
        playerScoreInAMatch.setBallsFaced(26);
        playerScoreInAMatch.setWicketsTaken(1);
        playerScoreInAMatch.setRunsGiven(23);
        playerScoreInAMatchList.add(playerScoreInAMatch);
        ScoreCard scoreCard=new ScoreCard();
        scoreCard.setPlayerScoreInAMatchList(playerScoreInAMatchList);
        Mockito.when(scorecardRepository.setScoreCard(scoreCard)).thenReturn(scoreCard);
        ScoreCard result=scorecardService.setScoreCard(scoreCard);
        assertEquals(scoreCard,result);

    }
}

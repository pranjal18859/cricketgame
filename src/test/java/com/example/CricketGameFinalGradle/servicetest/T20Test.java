package com.example.CricketGameFinalGradle.servicetest;

import com.example.CricketGameFinalGradle.services.serviceimplementation.T20;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class T20Test {
    @Test
    public void testT20() throws Exception{
        T20 t20=new T20();
        int expected=20;
        int actual= t20.getmatchFormat();
        assertEquals(expected,actual);

    }
}

package com.example.CricketGameFinalGradle.controller;

import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.services.serviceimplementation.TeamServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/cricketGame")
public class TeamController {

    @Autowired
    TeamServiceImpl teamServiceImpl;

    @PostMapping("/createTeam")
    public ResponseEntity<Team> createTeam(@RequestParam(value = "id") int teamId, @RequestParam String teamName) {
        try {
            return ResponseEntity.of(Optional.of(teamServiceImpl.createTeamById(teamName, teamId)));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}


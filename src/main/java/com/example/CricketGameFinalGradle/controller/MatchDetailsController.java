package com.example.CricketGameFinalGradle.controller;

import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.services.serviceimplementation.MatchDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



public class MatchDetailsController {
    @Autowired
    MatchDetailsServiceImpl matchDetailsServiceImpl;
    @GetMapping("/getWinningTeam")
    public Team getWinningTeam(@RequestParam int matchId) {
        try {
            return matchDetailsServiceImpl.getWinnerTeam(matchId);
        } catch (RuntimeException e) {
            System.err.println("An error occurred while getting the winning team: " + e.getMessage());
            return null;
        }
    }
}
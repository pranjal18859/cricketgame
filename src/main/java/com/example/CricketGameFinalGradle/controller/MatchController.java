package com.example.CricketGameFinalGradle.controller;
import com.example.CricketGameFinalGradle.models.Match;
import com.example.CricketGameFinalGradle.services.serviceimplementation.GameServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class MatchController {
    private GameServiceImpl gameServiceImpl;

    @Autowired
    public MatchController(GameServiceImpl gameService) {
        this.gameServiceImpl = gameService;
    }
    @PostMapping("/createMatch")
    public ResponseEntity<String> startMatch(@RequestBody Match match) throws Exception{
        try {
            String gameStatus = gameServiceImpl.gameService(match);
            return ResponseEntity.of(Optional.of("Match Done"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


}


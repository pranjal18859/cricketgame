package com.example.CricketGameFinalGradle.controller;

import com.example.CricketGameFinalGradle.models.Player;
import com.example.CricketGameFinalGradle.services.serviceimplementation.PlayerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cricketGame")
public class PlayerController {
    @Autowired
    PlayerServiceImpl playerServiceImpl;
    @GetMapping("/getPlayerById/{id}")
    public ResponseEntity<Player> getPlayerById(@PathVariable String id) throws Exception {
        return new ResponseEntity<>(playerServiceImpl.getPlayerById(Integer.parseInt(id)), HttpStatus.OK);
    }
    @PostMapping("/addPlayer")
    public ResponseEntity<Player> addPlayer(@RequestBody Player player) {
        return ResponseEntity.of(Optional.of(playerServiceImpl.addPlayer(player)));
    }

    @PostMapping("/addMultiplePlayers")
    public ResponseEntity<Collection<Player>> addMultiplePlayers(@RequestBody List<Player> players) {
        return ResponseEntity.of(Optional.of(playerServiceImpl.addMultiplePlayers(players)));
    }

    @GetMapping("/getAllPlayers")
    public ResponseEntity<List<Player>> getAllPlayers() {
        List<Player> players = playerServiceImpl.getAllPlayers();
        return ResponseEntity.of(Optional.of(players));
    }
}

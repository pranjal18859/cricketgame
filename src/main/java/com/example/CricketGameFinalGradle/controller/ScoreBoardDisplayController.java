package com.example.CricketGameFinalGradle.controller;

import com.example.CricketGameFinalGradle.view.ScoreBoard;
import com.example.CricketGameFinalGradle.view.ScoreBoardUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/stats")
public class ScoreBoardDisplayController {

    @GetMapping("/battingTeamScoreBoard")
    public ResponseEntity<String> getBattingTeamScoreBoard(){
        try {
            ScoreBoard scoreBoard=new ScoreBoard();
            return ResponseEntity.of(Optional.of(scoreBoard.getBattingTeamScoreBoard()));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/bowlingTeamScoreBoard")
    public ResponseEntity<String> getBowlingTeamScoreBoard(){
        try {
            ScoreBoard scoreBoard=new ScoreBoard();
            return ResponseEntity.of(Optional.of(scoreBoard.getBowlingTeamScoreBoard()));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/getResult")
    public ResponseEntity<String> getResult(){
        try {
            ScoreBoard scoreBoard=new ScoreBoard();
            return ResponseEntity.of(Optional.of(scoreBoard.getResult()));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}


package com.example.CricketGameFinalGradle.exception;

public class TeamBuilderException extends RuntimeException{
    public TeamBuilderException(String message) {
        super(message);
    }
}

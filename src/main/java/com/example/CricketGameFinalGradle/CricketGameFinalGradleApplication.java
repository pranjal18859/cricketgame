package com.example.CricketGameFinalGradle;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import java.util.logging.Logger;

@SpringBootApplication
@EnableMongoRepositories(basePackages = "com.example.CricketGameFinalGradle.repository.mongorepo")
public class CricketGameFinalGradleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CricketGameFinalGradleApplication.class, args);

	}

}

package com.example.CricketGameFinalGradle.services.servicesinterface;

import com.example.CricketGameFinalGradle.models.Team;

public interface TeamService {
    public Team getTeamById(Integer id);

    public Team createTeamById(String teamName, Integer id);

}

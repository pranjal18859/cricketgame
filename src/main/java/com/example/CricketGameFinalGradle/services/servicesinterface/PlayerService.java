package com.example.CricketGameFinalGradle.services.servicesinterface;

import com.example.CricketGameFinalGradle.models.Player;

import java.util.Collection;
import java.util.List;

public interface PlayerService {
    public Player getPlayerById(Integer id) throws Exception ;

    public Player addPlayer(Player player);
    public Collection<Player> addMultiplePlayers(List<Player> players);
    public List<Player> getAllPlayers();



}

package com.example.CricketGameFinalGradle.services.servicesinterface;

import com.example.CricketGameFinalGradle.models.Player;

import java.util.ArrayList;

public interface StrikeChange{
    public abstract ArrayList<Player> changeStrike(Player P1, Player P2);
}

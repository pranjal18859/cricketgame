package com.example.CricketGameFinalGradle.services.servicesinterface;

import com.example.CricketGameFinalGradle.models.Player;
import com.example.CricketGameFinalGradle.models.Team;

import java.util.ArrayList;

public interface TeamBuilder {
    public ArrayList<Player> getTeamPlayers(Integer teamId);

}

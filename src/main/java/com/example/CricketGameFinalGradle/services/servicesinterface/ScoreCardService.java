package com.example.CricketGameFinalGradle.services.servicesinterface;

import com.example.CricketGameFinalGradle.models.ScoreCard;

public interface ScoreCardService {
    public ScoreCard setScoreCard(ScoreCard scoreCard);
}

package com.example.CricketGameFinalGradle.services.servicesinterface;

import com.example.CricketGameFinalGradle.models.Team;

public interface SecondInning {
    public int startTheSecondInning(Team firstTeam, Team secondTeam, int overs, int targetScore);
}

package com.example.CricketGameFinalGradle.services.servicesinterface;

import com.example.CricketGameFinalGradle.models.Team;

public interface CricketService {
    public Team createFirstTeam(String name, int teamId);

}

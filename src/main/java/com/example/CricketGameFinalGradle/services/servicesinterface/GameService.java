package com.example.CricketGameFinalGradle.services.servicesinterface;

import com.example.CricketGameFinalGradle.models.Match;
import org.springframework.stereotype.Service;

@Service
public interface GameService {
    public String gameService(Match match);
}

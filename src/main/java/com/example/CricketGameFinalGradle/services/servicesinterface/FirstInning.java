package com.example.CricketGameFinalGradle.services.servicesinterface;

import com.example.CricketGameFinalGradle.models.Team;

public interface FirstInning {
    public int startTheFirstInning(Team firstTeam, Team secondTeam, int overs);
}

package com.example.CricketGameFinalGradle.services.servicesinterface;

import com.example.CricketGameFinalGradle.models.Team;

public interface MatchDetailsService {
    public Team getWinnerTeam(int matchId);
}

package com.example.CricketGameFinalGradle.services.serviceimplementation;


import com.example.CricketGameFinalGradle.services.servicesinterface.MatchDate;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MatchDateImpl implements MatchDate {
    public  void displayMatchDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        System.out.println(formatter.format(date));

    }
}


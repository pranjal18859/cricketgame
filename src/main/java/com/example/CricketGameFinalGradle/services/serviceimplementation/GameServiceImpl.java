package com.example.CricketGameFinalGradle.services.serviceimplementation;

import com.example.CricketGameFinalGradle.models.Match;
import com.example.CricketGameFinalGradle.services.servicesinterface.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameServiceImpl implements GameService {
    @Autowired
    MatchStarter matchStarter;


    public String gameService(Match match){
        matchStarter.play(match);
        return "Game has completed";

    }
}

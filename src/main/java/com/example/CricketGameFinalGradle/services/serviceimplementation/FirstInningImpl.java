package com.example.CricketGameFinalGradle.services.serviceimplementation;

import com.example.CricketGameFinalGradle.models.Player;
import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.services.servicesinterface.FirstInning;
import com.example.CricketGameFinalGradle.services.servicesinterface.StrikeChange;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.function.Supplier;

@Service
public class FirstInningImpl implements FirstInning {
    public int startTheFirstInning(Team firstTeam, Team secondTeam, int overs){
        GenerateRunsImpl generateRunsImpl =new GenerateRunsImpl();
        int[] batsmanNumber={0};
        int[] bowlerNumber={-1};
        int[] finalBatsmanNumber = batsmanNumber;
        Supplier<Player> getStrikerBatsman=()->firstTeam.getBatsman(finalBatsmanNumber[0]++);
        Player strikerBatsman=getStrikerBatsman.get();
        int[] finalBatsmanNumber1 = batsmanNumber;
        Supplier<Player> getNonStrikerBatsman = () -> firstTeam.getBatsman(finalBatsmanNumber1[0]++);
        Player nonStrikerBatsman = getNonStrikerBatsman.get();

        StrikeChange strikeChangeImpl = (p1, p2) -> {
            ArrayList<Player> newPlayer = new ArrayList<>();
            newPlayer.add(p2);
            newPlayer.add(p1);
            return newPlayer;
        };
        for(int over=0;over<overs;over++){

            int newBowler= secondTeam.nextBowler(bowlerNumber[0]);
            Player bowler = secondTeam.getBowler(newBowler);
            bowlerNumber[0] = newBowler;
            for(int bowl=0;bowl<6;bowl++){
                int score= generateRunsImpl.generateRandom();
                if(score==7){
                    bowler.addWickets();
                    firstTeam.addTotalWicketLoss();
                    if(firstTeam.getTotalWicketLoss()==10){
                        return firstTeam.getTeamScore();
                    }
                    strikerBatsman=getStrikerBatsman.get();
                }
                else{
                    strikerBatsman.addRuns(score);
                    bowler.addRunsGiven(score);
                    firstTeam.addTotalScore(score);
                    if(score%2!=0){
                        ArrayList<Player> position = strikeChangeImpl.changeStrike(strikerBatsman, nonStrikerBatsman);
                        strikerBatsman = position.get(0);
                        nonStrikerBatsman = position.get(1);
                    }
                }
                firstTeam.addOvers();
                ArrayList<Player> position = strikeChangeImpl.changeStrike(strikerBatsman, nonStrikerBatsman);
                strikerBatsman = position.get(0);
                nonStrikerBatsman = position.get(1);
            }
        }
        return firstTeam.getTeamScore();
    }


}
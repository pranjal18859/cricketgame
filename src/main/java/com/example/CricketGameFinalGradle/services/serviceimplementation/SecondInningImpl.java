package com.example.CricketGameFinalGradle.services.serviceimplementation;

import com.example.CricketGameFinalGradle.models.Player;
import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.services.servicesinterface.SecondInning;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
@Service
public class SecondInningImpl implements SecondInning {
    public int startTheSecondInning(Team firstTeam, Team secondTeam, int overs, int targetScore) {
        GenerateRunsImpl generateRunsImpl = new GenerateRunsImpl();
        int batsmanNumber = 0;
        int bowlerNumber = -1;
        Player strikerBatsman = firstTeam.getBatsman(batsmanNumber++);
        Player nonStrikerBatsman = firstTeam.getBatsman(batsmanNumber++);
        int totalScore = 0;
        for (int over = 0; over < overs; over++) {
            int newBowler = secondTeam.nextBowler(bowlerNumber);
            Player bowler = secondTeam.getBowler(newBowler);
            bowlerNumber = newBowler;

            for (int bowl = 0; bowl < 6; bowl++) {
                int score = generateRunsImpl.generateRandom();
                if (score == 7) {
                    bowler.addWickets();
                    firstTeam.addTotalWicketLoss();
                    if (firstTeam.getTotalWicketLoss() == 10) {
                        return totalScore;
                    }
                    strikerBatsman = firstTeam.getBatsman(batsmanNumber++);
                } else {
                    strikerBatsman.addRuns(score);
                    bowler.addRunsGiven(score);
                    firstTeam.addTotalScore(score);
                    totalScore += score;
                    if (score % 2 != 0) {
                        StrikeChangeImpl strikeChangeImpl = new StrikeChangeImpl();
                        ArrayList<Player> position = strikeChangeImpl.changeStrike(strikerBatsman, nonStrikerBatsman);
                        strikerBatsman = position.get(0);
                        nonStrikerBatsman = position.get(1);
                    }
                }
                firstTeam.addOvers();
                StrikeChangeImpl strikeChangeImpl = new StrikeChangeImpl();
                ArrayList<Player> position = strikeChangeImpl.changeStrike(strikerBatsman, nonStrikerBatsman);
                strikerBatsman = position.get(0);
                nonStrikerBatsman = position.get(1);

                if (totalScore >= targetScore) {
                    return totalScore;
                }
            }
        }
        return totalScore;
    }
}

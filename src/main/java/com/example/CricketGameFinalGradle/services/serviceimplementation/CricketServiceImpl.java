package com.example.CricketGameFinalGradle.services.serviceimplementation;

import com.example.CricketGameFinalGradle.models.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CricketServiceImpl {
    @Autowired
    private TeamBuilderImpl teamBuilder;

    public Team createFirstTeam(String name, int teamId){
        return teamBuilder.getTeam(name, teamId);
    }
}

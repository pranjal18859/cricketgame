
package com.example.CricketGameFinalGradle.services.serviceimplementation;
import com.example.CricketGameFinalGradle.models.*;
import com.example.CricketGameFinalGradle.repository.esrepo.ElasticMatchRepo;
import com.example.CricketGameFinalGradle.repository.mongorepo.MatchRepository;
import com.example.CricketGameFinalGradle.view.ScoreBoard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class MatchStarter {
    @Autowired
    ElasticMatchRepo elasticMatchRepo;
    @Autowired
    private TeamServiceImpl teamServiceImpl;
    @Autowired
    private MatchRepository matchRepository;
    @Autowired
    private FirstInningImpl firstInning;
    @Autowired
    private SecondInningImpl secondInning;
    private Team firstTeam;
    private Team secondTeam;
    public ArrayList<Player> getFirstTeamPlayers() {
        return firstTeamPlayers;
    }
    private ArrayList<Player> firstTeamPlayers;
    private ArrayList<Player> secondTeamPlayers;
    public Team getFirstTeam() {
        return firstTeam;
    }
    public void play(Match match){
        MatchFactory matchFactory=new MatchFactory();
        int overs= matchFactory.getmatchFormat(match.getGameFormat()).getmatchFormat();
        matchRepository.createMatch(match);
        Team firstTeam = teamServiceImpl.getTeamById(match.getFirstTeamId());
        Team secondTeam = teamServiceImpl.getTeamById(match.getSecondTeamId());
        System.out.println(firstTeam);
        System.out.println(secondTeam);

        Team winningTeam;
        if(TossServicesImpl.toss()==1){
            System.out.println(firstTeam.getTeamName()+" has won the Toss and elected to bat first");
            int firstTeamScore=firstInning.startTheFirstInning(firstTeam, secondTeam,overs);
            int secondTeamScore= secondInning.startTheSecondInning(secondTeam, firstTeam, overs, firstTeamScore + 1);
            winningTeam = displayScoreBoard(firstTeam, secondTeam);
        }
        else{
            System.out.println(secondTeam.getTeamName()+" has won the Toss and elected to bat first");
            int secondTeamScore= firstInning.startTheFirstInning(secondTeam, firstTeam, overs);
            int firstTeamScore= secondInning.startTheSecondInning(firstTeam, secondTeam,overs,secondTeamScore+1);
            winningTeam = displayScoreBoard(secondTeam, firstTeam);
        }
        ScoreCard firstTeamScoreCard = getScoreCard(firstTeam);
        ScoreCard secondTeamScoreCard = getScoreCard(secondTeam);
        matchRepository.setMatchWinnerAndScoreCard(match.getId(), winningTeam.getTeamId(), firstTeamScoreCard, secondTeamScoreCard);
        Match newMatch=matchRepository.getMatchById(match.getId());
        elasticMatchRepo.save(newMatch);
    }
    public ArrayList<Player> getTeam1Players() {
        return firstTeam.getPlayers();
    }

    public ArrayList<Player> getTeam2Players() {
        return secondTeam.getPlayers();
    }
    private ScoreCard getScoreCard(Team team) {
        List<PlayerScoreInAMatch> playerScoreInAMatchList = new ArrayList<>();
        for (Player player:team.getPlayers()) {
            playerScoreInAMatchList.add(new PlayerScoreInAMatch(player.getId(),player.getPlayerName(),player.getRunsGiven(), player.getWicketsTaken(),
                    player.getBallsFaced(), player.getRunsScored()));
        }
        return new ScoreCard(playerScoreInAMatchList);
    }
    private Team displayScoreBoard(Team firstTeam, Team secondTeam){
        ScoreBoard scoreBoard=new ScoreBoard();
        scoreBoard.displayScoreBoardOfBattingTeam(firstTeam);
        scoreBoard.displayScoreBoardOfBowlingTeam(secondTeam);
        return scoreBoard.displayResult(firstTeam, secondTeam);
    }
}
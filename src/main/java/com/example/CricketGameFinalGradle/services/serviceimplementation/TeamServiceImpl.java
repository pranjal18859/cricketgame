package com.example.CricketGameFinalGradle.services.serviceimplementation;

import com.example.CricketGameFinalGradle.exception.NotFoundException;
import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.repository.mongorepo.TeamRepository;
import com.example.CricketGameFinalGradle.services.servicesinterface.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamServiceImpl implements TeamService {
    @Autowired
    TeamRepository teamRepository;
    public Team getTeamById(Integer id){
        if(id==null){
            throw new NotFoundException("Team with given id not exists");
        }
        Team team = teamRepository.getTeamById(id);
        return team;
    }
    public Team createTeamById(String teamName, Integer id) {
        return teamRepository.createTeamById(teamName, id);
    }
}

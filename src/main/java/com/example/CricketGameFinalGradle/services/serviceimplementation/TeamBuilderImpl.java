package com.example.CricketGameFinalGradle.services.serviceimplementation;
import com.example.CricketGameFinalGradle.enums.PlayerRole;
import com.example.CricketGameFinalGradle.exception.TeamBuilderException;
import com.example.CricketGameFinalGradle.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TeamBuilderImpl{
    @Autowired
    private PlayerServiceImpl playerServiceImpl;
    public Team getTeam(String teamName, Integer teamId) {
        List<Player> players = playerServiceImpl.getAllPlayers();
        ArrayList<Player> batterPlayersInTheTeam = new ArrayList<>();
        ArrayList<Player> bowlerPlayersInTheTeam = new ArrayList<>();
        ArrayList<Player> allPlayers = new ArrayList<>();
        for (Player player:players) {
            if (player.getTeamId() == teamId && player.getPlayerRole().equals(PlayerRole.BATSMAN)) {
                batterPlayersInTheTeam.add(player);
                allPlayers.add(player);
                if(batterPlayersInTheTeam.size() == 6) break;
            }
        }
        for (Player player:players) {
            if (player.getTeamId() == teamId && player.getPlayerRole().equals(PlayerRole.BOWLER)) {
                bowlerPlayersInTheTeam.add(player);
                allPlayers.add(player);
                if(bowlerPlayersInTheTeam.size() == 5) break;
            }
        }
        if(allPlayers.size()<11){
            throw new TeamBuilderException("The team does not have enough players to play");
        }
        Team team = new Team(teamName, allPlayers);
        team.setTeamId(teamId);
        return team;

    }
}








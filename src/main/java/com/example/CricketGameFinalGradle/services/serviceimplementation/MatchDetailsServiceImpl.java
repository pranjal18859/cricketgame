package com.example.CricketGameFinalGradle.services.serviceimplementation;

import com.example.CricketGameFinalGradle.models.Match;
import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.repository.mongorepo.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MatchDetailsServiceImpl {
    @Autowired
    MatchRepository matchRepository;

    public Team getWinnerTeam(int matchId) {
        return matchRepository.getWinningTeam(matchId);
    }
}

package com.example.CricketGameFinalGradle.services.serviceimplementation;

import com.example.CricketGameFinalGradle.models.ScoreCard;
import com.example.CricketGameFinalGradle.repository.mongorepo.ScorecardRepository;
import com.example.CricketGameFinalGradle.services.servicesinterface.ScoreCardService;
import org.springframework.beans.factory.annotation.Autowired;

public class ScorecardServiceImpl implements ScoreCardService {
    @Autowired
    ScorecardRepository scorecardRepository;
    public ScoreCard setScoreCard(ScoreCard scoreCard) {
        return scorecardRepository.setScoreCard(scoreCard);
    }
}

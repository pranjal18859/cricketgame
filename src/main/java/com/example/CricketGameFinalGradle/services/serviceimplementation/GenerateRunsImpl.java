package com.example.CricketGameFinalGradle.services.serviceimplementation;

import com.example.CricketGameFinalGradle.services.servicesinterface.GenerateRuns;

public class GenerateRunsImpl implements GenerateRuns {
    public int generateRandom(){
        int outcome=(int)(Math.random()*100);
        if(outcome>85) return 7;
        else if(outcome>70) return 6;
        else if(outcome>60) return 4;
        else return (int)(Math.random()*5);
    }
}

package com.example.CricketGameFinalGradle.services.serviceimplementation;

import com.example.CricketGameFinalGradle.services.servicesinterface.MatchFormat;

public class MatchFactory {
    public MatchFormat getmatchFormat(String input){
        if("T20".equals(input)) return new T20();
        else if("ODI".equals(input)) return new ODI();
        else if("RAIN".equals(input)) return new DuckworthLewis();
        else return null;
    }
}

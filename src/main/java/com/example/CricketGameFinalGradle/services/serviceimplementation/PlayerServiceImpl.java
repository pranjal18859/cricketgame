package com.example.CricketGameFinalGradle.services.serviceimplementation;

import com.example.CricketGameFinalGradle.exception.NotFoundException;
import com.example.CricketGameFinalGradle.models.Player;
import com.example.CricketGameFinalGradle.repository.esrepo.ElasticPlayerRepo;
import com.example.CricketGameFinalGradle.repository.mongorepo.PlayerRepository;
import com.example.CricketGameFinalGradle.services.servicesinterface.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class PlayerServiceImpl implements PlayerService {
    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    ElasticPlayerRepo elasticPlayerRepo;
    public Player getPlayerById(Integer id) throws Exception {
        Player player = playerRepository.getPlayerById(id);
        if (player == null) {
            throw new NotFoundException("Player with the given id does not exist");
        }
        return player;
    }
    public Player addPlayer(Player player) {
        elasticPlayerRepo.save(player);
        return playerRepository.addPlayer(player);
    }
    public Collection<Player> addMultiplePlayers(List<Player> players) {
        elasticPlayerRepo.saveAll(players);
        return playerRepository.addMultiplePlayers(players);
    }
    public List<Player> getAllPlayers() {
        return playerRepository.getAllPlayers();
    }
}

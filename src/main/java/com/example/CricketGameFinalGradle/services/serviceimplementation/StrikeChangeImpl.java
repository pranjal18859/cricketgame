package com.example.CricketGameFinalGradle.services.serviceimplementation;

import com.example.CricketGameFinalGradle.models.Player;
import com.example.CricketGameFinalGradle.services.servicesinterface.StrikeChange;

import java.util.ArrayList;

public class StrikeChangeImpl implements StrikeChange {
    public ArrayList<Player> changeStrike(Player P1, Player P2){
        ArrayList<Player> newPlayer=new ArrayList<Player>();
        newPlayer.add(P2);
        newPlayer.add(P1);
        return newPlayer;
    }

}

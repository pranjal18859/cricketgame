package com.example.CricketGameFinalGradle.models;

import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("Matches")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@org.springframework.data.elasticsearch.annotations.Document(indexName = "match_stats")
public class Match {
    @Id
    private Integer id;
    private Integer firstTeamId;
    private Integer secondTeamId;
    private Integer winnerTeamId;
    private String gameFormat;
    private ScoreCard firstTeamScoreCard;
    private ScoreCard secondTeamScoreCard;





}

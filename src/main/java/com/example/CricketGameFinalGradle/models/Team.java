package com.example.CricketGameFinalGradle.models;



import com.example.CricketGameFinalGradle.enums.PlayerRole;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
@Data
@Document("Teams")
@NoArgsConstructor
@AllArgsConstructor
public class Team{

    @Id
    private Integer teamId;

    private String teamName;

    private int teamScore;

    private int totalWicketLoss;

    private int totalOversPlayed;

    private ArrayList<Player> Players;
    public void addTotalScore(int runs){
        teamScore+=runs;
    }
    public void addTotalWicketLoss(){
        totalWicketLoss+=1;
    }
    public void addOvers(){
        totalOversPlayed+=1;
    }

    private ArrayList<Player> bowlers;
    public ArrayList<Player> getBowlers(){
        Players=getPlayers();
        bowlers=new ArrayList<Player>();
        for(Player p:Players){
            if(p.getPlayerRole().equals(PlayerRole.BOWLER)){
                bowlers.add(p);
            }
        }
        return bowlers;
    }
    public Player getBatsman(int batsmanNumber){
        return Players.get(batsmanNumber);
    }
    public int nextBowler(int previousBaller){
        int nextBowler =(int)(Math.random()*4);
        while(previousBaller== nextBowler){
            nextBowler =(int)(Math.random()*4);
        }
        return nextBowler;
    }
    public Player getBowler(int bowlerNumber){

        return bowlers.get(bowlerNumber);
    }
    public Team(String teamName, ArrayList<Player> players){
        this.teamName = teamName;
        this.Players=players;
        totalOversPlayed=0;
        teamScore=0;
        totalWicketLoss=0;
        bowlers=getBowlers();

    }
    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

}


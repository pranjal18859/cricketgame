package com.example.CricketGameFinalGradle.models;
import com.example.CricketGameFinalGradle.enums.PlayerRole;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;
@Data
@Document("player")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@CompoundIndex(name = "playerRole",def = "{'playerRole':1}")
@CompoundIndex(name="playerName",def ="{'playerName':1}")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "elastic_player")
public class Player {
    private String playerName;
    @Id
    private int id;
    private int teamId;
    private int wicketsTaken;
    private int runsGiven;
    private PlayerRole playerRole;
    private int ballsFaced;
    private int runsScored;

    public Player(String playerName, @NonNull PlayerRole playerRole, int ballsFaced, int runsScored, int wicketsTaken, int i, int i1) {
        this.playerName = playerName;
        this.playerRole = playerRole;
        this.ballsFaced = ballsFaced;
        this.runsScored = runsScored;
        this.wicketsTaken=wicketsTaken;
    }
    public void addRuns(int runs){
        runsScored+=runs;
        ballsFaced+=1;
    }
    public void wicketLoss(){
        ballsFaced+=1;
    }
    public void addWickets(){
        wicketsTaken+=1;
    }
    public void addRunsGiven(int runs){
        runsGiven+=runs;
    }
    public Player(String playerName,PlayerRole playerRole){
        this.playerName=playerName;
        this.playerRole=playerRole;
        runsScored=0;
        ballsFaced=0;
        wicketsTaken=0;
        runsGiven=0;
    }

}


package com.example.CricketGameFinalGradle.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document("playerScoreInAMatch")
public class PlayerScoreInAMatch {

    private int playerId;
    private String playerName;
    private int runsGiven;
    private int wicketsTaken;
    private int ballsFaced;
    private int runsScored;
}

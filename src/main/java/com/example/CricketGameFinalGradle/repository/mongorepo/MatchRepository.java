package com.example.CricketGameFinalGradle.repository.mongorepo;

import com.example.CricketGameFinalGradle.models.Match;
import com.example.CricketGameFinalGradle.models.ScoreCard;
import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.repository.esrepo.ElasticPlayerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class MatchRepository {
    @Autowired
    MongoTemplate mongoTemplate;


    public Match createMatch(Match match) {

        return mongoTemplate.save(match);
    }
    public Match getMatchById(Integer id) {
        Query query = new Query(Criteria.where("id").is(id));
        return mongoTemplate.findOne(query, Match.class);
    }
    public Match setMatchWinnerAndScoreCard(Integer matchId, Integer winnerTeamId, ScoreCard firstTeamScoreCard,
                                            ScoreCard secondTeamScoreCard) {
        Query query = new Query(Criteria.where("id").is(matchId));
        Update updateDefinition = new Update().set("winnerTeamId", winnerTeamId)
                .set("firstTeamScoreCard", firstTeamScoreCard).set("secondTeamScoreCard", secondTeamScoreCard);
        FindAndModifyOptions findAndModifyOptions = new FindAndModifyOptions().returnNew(true);
        Match match = mongoTemplate.findAndModify(query, updateDefinition, findAndModifyOptions, Match.class);
        return match;
    }
    public Team getWinningTeam(int matchId) {
        Query query = new Query(Criteria.where("id").is(matchId));
        Match match = mongoTemplate.findOne(query, Match.class);
        int winningTeamId = match.getWinnerTeamId();
        query = new Query(Criteria.where("teamId").is(winningTeamId));
        return mongoTemplate.findOne(query, Team.class);
    }
}

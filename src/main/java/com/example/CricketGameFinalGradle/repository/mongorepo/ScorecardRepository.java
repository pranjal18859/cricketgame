package com.example.CricketGameFinalGradle.repository.mongorepo;

import com.example.CricketGameFinalGradle.models.ScoreCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

public class ScorecardRepository {
    @Autowired
    MongoTemplate mongoTemplate;
    public ScoreCard setScoreCard(ScoreCard scoreCard) {
        return mongoTemplate.save(scoreCard);
    }
}

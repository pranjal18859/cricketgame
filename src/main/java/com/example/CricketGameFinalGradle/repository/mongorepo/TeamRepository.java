package com.example.CricketGameFinalGradle.repository.mongorepo;

import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.services.serviceimplementation.TeamBuilderImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class TeamRepository {
    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    TeamBuilderImpl teamBuilder;
    public Team getTeamById(Integer teamId) {
        Query query = new Query(Criteria.where("teamId").is(teamId));
        Team team =  mongoTemplate.findOne(query, Team.class);
        return team;
    }
    public Team createTeamById(String teamName, int teamId) {
        Team team = teamBuilder.getTeam(teamName, teamId);
        return mongoTemplate.save(team);
    }
}

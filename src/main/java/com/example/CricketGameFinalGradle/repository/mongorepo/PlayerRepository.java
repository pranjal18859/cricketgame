package com.example.CricketGameFinalGradle.repository.mongorepo;

import com.example.CricketGameFinalGradle.models.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public class PlayerRepository {
    @Autowired
    MongoTemplate mongoTemplate;
    public Player getPlayerById(Integer id) {
        Query query = new Query(Criteria.where("id").is(id));
        return mongoTemplate.findOne(query, Player.class);
    }
    public Player addPlayer(Player player) {
        return mongoTemplate.save(player);
    }
    public List<Player> getAllPlayers() {
        return mongoTemplate.findAll(Player.class);
    }
    public Collection<Player> addMultiplePlayers(List<Player> players) {
        return mongoTemplate.insertAll(players);
    }

}

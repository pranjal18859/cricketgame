package com.example.CricketGameFinalGradle.repository.esrepo;


import com.example.CricketGameFinalGradle.models.Player;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ElasticPlayerRepo extends ElasticsearchRepository<Player,Integer> {
}

package com.example.CricketGameFinalGradle.repository.esrepo;
import com.example.CricketGameFinalGradle.models.Match;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface ElasticMatchRepo extends ElasticsearchRepository<Match,Integer> {
}

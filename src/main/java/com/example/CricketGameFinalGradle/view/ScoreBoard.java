package com.example.CricketGameFinalGradle.view;

import com.example.CricketGameFinalGradle.models.Team;
import com.example.CricketGameFinalGradle.services.serviceimplementation.AnimationImpl;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class ScoreBoard {

    public static ArrayList<String> Table1=new ArrayList<>();
    public static ArrayList<String> Table2=new ArrayList<>();
    public static String winner;
    public static String margin;

    public static void displayScoreBoardOfBattingTeam(Team battingTeam){
        AnimationImpl.printMsgWithProgressBar("Loading ScoreBoard for Batting Team",25,60);
        System.out.println();
        System.out.println(battingTeam.getTeamName()+" has scored "+ battingTeam.getTeamScore()+" runs at loss of "+ battingTeam.getTotalWicketLoss());
        ScoreBoardUtil viewScoreBoard=new ScoreBoardUtil();
        viewScoreBoard.displayScoreBoard(battingTeam);
        Table1.add(battingTeam.getTeamName());
        Table1.add(String.valueOf(battingTeam.getTeamScore()));
        Table1.add(String.valueOf(battingTeam.getTotalOversPlayed()));
        Table1.add(String.valueOf(battingTeam.getTotalWicketLoss()));

    }

    public void displayScoreBoardOfBowlingTeam(Team bowlingTeam){
        AnimationImpl.printMsgWithProgressBar("Loading Scoreboard for Bowling Team",25,60);
        System.out.println();
        System.out.println(bowlingTeam.getTeamName()+" has scored "+ bowlingTeam.getTeamScore()+" runs at loss of "+ bowlingTeam.getTotalWicketLoss());
        ScoreBoardUtil viewScoreBoard=new ScoreBoardUtil();
        viewScoreBoard.displayScoreBoard(bowlingTeam);
        Table2.add(bowlingTeam.getTeamName());
        Table2.add(String.valueOf(bowlingTeam.getTeamScore()));
        Table2.add(String.valueOf(bowlingTeam.getTotalOversPlayed()));
        Table2.add(String.valueOf(bowlingTeam.getTotalWicketLoss()));

    }
    public Team displayResult(Team battingTeam, Team bowlingTeam){
        int battingTeamScore= battingTeam.getTeamScore();
        int bowlingTeamScore= bowlingTeam.getTeamScore();
        if(battingTeamScore>bowlingTeamScore){
            System.out.println(battingTeam.getTeamName()+" has won by "+(battingTeamScore-bowlingTeamScore)+" runs");
            winner= battingTeam.getTeamName();
            margin=String.valueOf(battingTeamScore-bowlingTeamScore);
            margin=margin+" runs";
            return battingTeam;
        }
        else if(battingTeamScore<bowlingTeamScore){
            System.out.println(bowlingTeam.getTeamName()+" has won by "+ (11- bowlingTeam.getTotalWicketLoss())+" wickets");
            winner= bowlingTeam.getTeamName();
            margin=String.valueOf(10- bowlingTeam.getTotalWicketLoss());
            margin=margin+"  wickets";
            return bowlingTeam;
        }
        return null;
    }
    public String getBattingTeamScoreBoard(){
        //System.out.println(Table1);
        return Table1.get(0)+" "+ Table1.get(1)+" "+ Table1.get(2)+" "+ Table1.get(3);
    }
    public String getBowlingTeamScoreBoard(){
        return Table2.get(0)+" "+Table2.get(1)+" "+Table2.get(2)+" "+Table2.get(3);

    }
    public String getResult(){
        String Margin=String.valueOf(margin);
        return "Winner of Match was  "+winner+" and won the match by "+margin;
    }



}


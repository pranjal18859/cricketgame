package com.example.CricketGameFinalGradle.view;

import java.util.ArrayList;

import com.example.CricketGameFinalGradle.models.*;
import com.example.CricketGameFinalGradle.services.serviceimplementation.DesignImpl;

public class ScoreBoardUtil {
    public String PlayerOfTheMatch;
        public void displayScoreBoard(Team team){
            ArrayList<Player> Players= team.getPlayers();
            System.out.println("---------------------------------------------------------------------------------------------------------------------");
            System.out.printf("%22s %20s %20s %20s %20s %20s", DesignImpl.ANSI_BLUE+"Name", DesignImpl.ANSI_BLUE+"Role", DesignImpl.ANSI_BLUE+"Runs Scored", DesignImpl.ANSI_BLUE+ "Balls Faced", DesignImpl.ANSI_BLUE+"Wickets Taken", DesignImpl.ANSI_BLUE+"Runs Given");
            System.out.println();
            System.out.println("---------------------------------------------------------------------------------------------------------------------");

            for (int i = 0; i < 11; ++i) {
                Player player = Players.get(i);
                System.out.format("%22s %20s %20s %20s %20s %20s", DesignImpl.ANSI_BLUE+player.getPlayerName(), DesignImpl.ANSI_BLUE+player.getPlayerRole(), DesignImpl.ANSI_BLUE+player.getRunsScored(), DesignImpl.ANSI_BLUE+player.getBallsFaced(), DesignImpl.ANSI_BLUE+player.getWicketsTaken(), DesignImpl.ANSI_BLUE+player.getRunsGiven());
                System.out.println();
            }
            System.out.println("---------------------------------------------------------------------------------------------------------------------");
        }
        public String getPlayerOfTheMatch(){
            return PlayerOfTheMatch;
        }

    }


